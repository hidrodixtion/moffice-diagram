@startuml

RealmSwift <|-- Article
RealmSwift <|-- Contact
RealmSwift <|-- Idea
RealmSwift <|-- Magazine
RealmSwift <|-- News
RealmSwift <|-- AppNotification
RealmSwift <|-- LoginData

@enduml
