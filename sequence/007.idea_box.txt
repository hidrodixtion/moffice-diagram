@startuml

actor User

User -> App : DoOpenApp

User -> App : <<login>>

User -> App : Tap 'Idea Box' Menu
activate App

App -> Server : getIdeaData("top_rated")
activate Server

Server --> App : <<response>>

App -> App : updateIdeaData("top_rated")

App -> Server : getIdeaData("newest")

Server --> App : <<response>>
deactivate Server

App -> App : updateIdeaData("newest")

App -> App : updateIdeaData("people")

App -> App : updateIdeaData("planet")

App -> App : updateIdeaData("profit")

App --> User : Done
deactivate App

@enduml
