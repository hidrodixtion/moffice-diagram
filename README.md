## README

This is supposed to be repository for all the diagram in moffice project.

For the best practice, diagram can be created using [PlantUML](http://plantuml.com/). Read the PlantUML docs, it's easy to create.

Example to build a sequence diagram :
`java -jar plantuml.jar -verbose 001.register.txt` 
